﻿using notificaciones.Models;

namespace notificaciones.Services
{
    public interface IServiceNotificacion
    {
        Task<IEnumerable<Notificacion>> GetAll();
        Task<Notificacion> AgregarNotificacionAsync(Notificacion nuevaNotificacion);
    }
}
