﻿using Microsoft.EntityFrameworkCore;
using notificaciones.Models;
using notificaciones.Repository;

namespace notificaciones.Services
{
    public class ServiceNotificacion : IServiceNotificacion
    {
        private readonly ContextDataBase _context;

        public ServiceNotificacion(ContextDataBase contexto)
        {
            
            _context = contexto;
        }

        public async Task<IEnumerable<Notificacion>> GetAll()
        {
            return await _context.Notificacion.ToListAsync();
        }

        public async Task<Notificacion> AgregarNotificacionAsync(Notificacion nuevaNotificacion)
        {
            try
            {
                _context.Notificacion.Add(nuevaNotificacion);
                await _context.SaveChangesAsync();
                return nuevaNotificacion;
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Error al agregar la notificación: {ex.Message}");
                return nuevaNotificacion;
            }
        }
    }
}
