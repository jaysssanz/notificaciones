﻿using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace notificaciones.Models
{
    public class Notificacion
    {
        [Key]
        public int Id { get; set; }

        [ForeignKey("IdCuenta")]
        public int IdCuenta { get; set; }

        public string Tipo { get; set; } = null!;

        public decimal Valor { get; set; }

        public string Cliente { get; set; } = null!;
    }
}
