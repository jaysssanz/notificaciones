﻿using notificaciones.Repository;

namespace notificaciones.Data
{
    public class DBBegin
    {
        public static void Initialize(ContextDataBase context)
        {
            context.Database.EnsureCreated();
            context.SaveChanges();
        }
    }
}
