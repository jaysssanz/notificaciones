﻿using Microsoft.AspNetCore.Mvc;
using notificaciones.DTOs;
using notificaciones.Models;
using notificaciones.Services;
using System.Threading.Tasks;

namespace notificaciones.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class NotificacionController : ControllerBase
    {
        private readonly IServiceNotificacion _notificacionService;

        public NotificacionController(IServiceNotificacion notificacionService)
        {
            _notificacionService = notificacionService;
        }

        [HttpGet]
        public async Task<ActionResult> Get()
        {
            return Ok(await _notificacionService.GetAll());
        }

        //[HttpPost("Agregar")]
        //public async Task<ActionResult> Agregar(NotificacionRequest nuevaNotificacion)
        //{
        //    try
        //    {
        //        var notificacion = new Notificacion
        //        {
        //            IdCuenta = nuevaNotificacion.IdCuentaR,
        //            Tipo = nuevaNotificacion.TipoR,
        //            Valor = nuevaNotificacion.ValorR,
        //            Cliente = nuevaNotificacion.ClienteR
        //        };

        //        var resultado = await _notificacionService.AgregarNotificacionAsync(notificacion);
        //        if (resultado)
        //        {
        //            return Ok("Notificación agregada con éxito.");
        //        }
        //        else
        //        {
        //            return StatusCode(500, "Error al agregar la notificación.");
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        return StatusCode(500, $"Error interno del servidor: {ex.Message}");
        //    }
        //}
    }
}

