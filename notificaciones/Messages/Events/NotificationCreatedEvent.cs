﻿using Cordillera.Distribuidas.Event.Events;

namespace historico.Messages.Events
{
    public class NotificationCreatedEvent : Event
    {
        public int AccountId { get; set; }
        public string Type { get; set; } = null!;
        public decimal Amount { get; set; }
        public string Customer { get; set; } = null!;

        public NotificationCreatedEvent(int accountId, string type, decimal amount, string customer)
        {
            AccountId = accountId;
            Type = type;
            Amount = amount;
            Customer = customer;

        }
    }
}
