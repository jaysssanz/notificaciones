﻿
using Cordillera.Distribuidas.Event.Bus;
using historico.Messages.Events;
using notificaciones.Models;
using notificaciones.Services;

namespace historico.Messages.EventHandler
{
    public class NotificationEventHandler : IEventHandler<NotificationCreatedEvent>
    {
        private readonly IServiceNotificacion _notificacionService;

        public NotificationEventHandler(IServiceNotificacion notificacionService)
        {
            _notificacionService = notificacionService;
        }

        public async Task Handle(NotificationCreatedEvent @event)
        {
            await _notificacionService.AgregarNotificacionAsync(new Notificacion()
            {
                IdCuenta = @event.AccountId,
                Tipo = @event.Type,
                Valor = @event.Amount,
                Cliente = @event.Customer,
            });
        }
    }
}
