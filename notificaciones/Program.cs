using Cordillera.Distribuidas.Event;
using Cordillera.Distribuidas.Event.Bus;
using depositos.Services;
using historico.Messages.EventHandler;
using historico.Messages.Events;
using MediatR;
using Microsoft.EntityFrameworkCore;
using notificaciones.Data;
using notificaciones.Repository;
using notificaciones.Services;
using System.Reflection;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.

builder.Services.AddControllers();

//NACOS
builder.Host.ConfigureAppConfiguration(cbuilder =>
{
    cbuilder.AddJsonFile("appsettings.docker.json", optional: false, reloadOnChange: true);
});

var nacosconfig = builder.Configuration.GetSection("nacosConfig");

builder.Host.ConfigureAppConfiguration((context, builder) =>
{
    // add nacos
    builder.AddNacosConfiguration(nacosconfig);
});


builder.Services.AddDbContext<ContextDataBase>(options =>
    options.UseMySQL(builder.Configuration["cn:mariadb"]));
//End Nacos


//Service datacontext Mysql

//builder.Services.AddDbContext<ContextDataBase>(options =>
//    options.UseMySQL(builder.Configuration.GetConnectionString("DefaultConnection")));

builder.Services.AddScoped<IServiceNotificacion, ServiceNotificacion>();
//builder.Services.AddSingleton<IHttpClient, CustomHttpClient>();


//RabbitMQ

builder.Services.AddMediatR(cfg => cfg.RegisterServicesFromAssembly(Assembly.GetExecutingAssembly()));
builder.Services.AddRabbitMQ();
builder.Services.AddTransient<NotificationEventHandler>();
builder.Services.AddTransient<IEventHandler<NotificationCreatedEvent>, NotificationEventHandler>();


var app = builder.Build();


//Create Database

using (var scope = app.Services.CreateScope())
{
    var services = scope.ServiceProvider;
    try
    {
        var context = services.GetRequiredService<ContextDataBase>();
        DBBegin.Initialize(context);
    }
    catch (Exception ex)
    {
        var logger = services.GetRequiredService<ILogger<Program>>();
        logger.LogError(ex, "An error occurred creating the DB.");
    }
}

// Configure the HTTP request pipeline.

app.UseAuthorization();

app.MapControllers();

//Configuracion de Bus
var eventBus = app.Services.GetRequiredService<IEventBus>();
eventBus.Subscribe<NotificationCreatedEvent, NotificationEventHandler>();

app.Run();
