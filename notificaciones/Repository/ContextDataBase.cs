﻿using Microsoft.EntityFrameworkCore;
using notificaciones.Models;
using System.Security.Principal;

namespace notificaciones.Repository
{
    public class ContextDataBase: DbContext
    {
        public ContextDataBase()
        {

        }

        public ContextDataBase(DbContextOptions<ContextDataBase> options)
            : base(options)
        {
        }

        public DbSet<Notificacion> Notificacion { get; set; }

    }
}
